# GC Import

A Python tool that imports a transaction file into a GnuCash database

Goals:

- allow importing the transactions from a file directly to GnuCash SQLite book
  - OFX file
  - MoneyManagerEx database
- have a CLI and GUI interfaces
- prevent creation of duplicates (?)

This tool is an extension of a series of tools assisting custom workflows with GnuCash as the accounting book.
It is the last step, which takes a generated transactions file and stores the transactions into the GnuCash SQLite3 database.

Caution is warranted. If you are not certain that the import files are valid, please use GnuCash import function in GnuCash software.

## File standard

### OFX

There is an OFX sample file in the `doc` folder, retrieved from [ofxstatement](https://github.com/kedder/ofxstatement) project.

OFX files can also be generated with [ofxtools](https://github.com/csingley/ofxtools).

### JSON

In case the existing standards, like OFX or QIF, are not enough, a custom file structure will be developed using JSON.

## MoneyManagerEx

MoneyManagerEx for Android is a decent mobile finance app that can be used to track transactions. Every now and then, these transactions would be transferred into GnuCash book and deleted from the mobile device.

