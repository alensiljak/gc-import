# OFX

There are OFX sample files in this folder.

`my_sample.ofx` contains only the minimum fields required for the purpose of this app. It uses the statement format.

## Transaction Fields

See section 11.4.4.1 <STMTTRN> in the 2.2 guide.

Possible values for TRNTYPE: http://ofx.net/downloads/OFX%202.2.pdf#G13.124534
Credit, Debit, and Xfer should suffice.

## Resources

- [OFX 2.2 guide](http://ofx.net/downloads/OFX%202.2.pdf)
- List of fields in transaction (STMTTRN) field: https://www.sapdatasheet.org/abap/tabl/ofx_stmttrn.html