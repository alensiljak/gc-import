# Demo

The /demo folder contains a test case with a GnuCash sqlite book.
One test case is a transaction created through GnuCash.
The second test case is a transaction created using piecash library.

## GnuCash Transaction

After creation of the transaction in GnuCash, there are the following records created:

- transaction (guid: d2...)
- 2 splits
  - "some bank"
  - "other account"
- 2 slots
  - "color" for 2a..., account "other account"
  - "date-posted" for d2..., transaction

## Piecash Transaction

The enter_date gets created in old format! Pull request with the fix sent.

Other than that, there are minor discrepancies in the saved default values but all the records are there:

- transaction
- 2 splits
- 1 slot
