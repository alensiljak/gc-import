"""
This script will create a transaction in the demo database in the same folder.
The purpose is to compared the data after a transaction is created through piecash.
"""

def main():
    """ The full test """
    import os
    import pathlib
    from datetime import date, datetime
    from piecash import Book, open_book, Split, Transaction

    #current_folder = os.getcwd()
    current_folder = pathlib.Path(__file__).parent
    db_path = os.path.join(current_folder, "demo.sql.gnucash")
    with open_book(db_path, readonly=False) as book:
        # currency
        currency = book.commodities(namespace="CURRENCY",mnemonic="EUR")
        today = datetime(datetime.now().year, 12, 18)

        acc1 = book.accounts(name="some bank")
        acc2 = book.accounts(name="other account")

        split1 = Split(account=acc1,value=-23)
        split2 = Split(account=acc2,value=23)
        tx = Transaction(currency=currency,
            description="python txfer",
            splits=[split1, split2],
            post_date=today.date(),
            enter_date=today)

        #book.transactions
        book.add(tx)
        book.save()

        print("done")

if __name__ == "__main__":
    main()
